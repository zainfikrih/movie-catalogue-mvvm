package men.ngopi.zain.moviecataloguemvvm.data.source.local.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class MovieEntity implements Parcelable {

    @PrimaryKey
    private int id;
    private double vote_average;
    private int vote_count;
    private String title;
    private String release_date;
    private String backdrop_path;
    private String overview;
    private String poster_path;
    private String original_name;
    private String first_air_date;
    private String original_language;
    private double popularity;

    public MovieEntity() {

    }

    @Ignore
    public MovieEntity(int id, double vote_average, int vote_count, String title, String release_date, String backdrop_path, String overview, String poster_path, String original_name, String first_air_date, String original_language, double popularity) {
        this.id = id;
        this.vote_average = vote_average;
        this.vote_count = vote_count;
        this.title = title;
        this.release_date = release_date;
        this.backdrop_path = backdrop_path;
        this.overview = overview;
        this.poster_path = poster_path;
        this.original_name = original_name;
        this.first_air_date = first_air_date;
        this.original_language = original_language;
        this.popularity = popularity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        this.original_name = original_name;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public void setFirst_air_date(String first_air_date) {
        this.first_air_date = first_air_date;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeDouble(this.vote_average);
        dest.writeInt(this.vote_count);
        dest.writeString(this.title);
        dest.writeString(this.release_date);
        dest.writeString(this.backdrop_path);
        dest.writeString(this.overview);
        dest.writeString(this.poster_path);
        dest.writeString(this.original_name);
        dest.writeString(this.first_air_date);
        dest.writeString(this.original_language);
        dest.writeDouble(this.popularity);
    }

    protected MovieEntity(Parcel in) {
        this.id = in.readInt();
        this.vote_average = in.readDouble();
        this.vote_count = in.readInt();
        this.title = in.readString();
        this.release_date = in.readString();
        this.backdrop_path = in.readString();
        this.overview = in.readString();
        this.poster_path = in.readString();
        this.original_name = in.readString();
        this.first_air_date = in.readString();
        this.original_language = in.readString();
        this.popularity = in.readDouble();
    }

    public static final Parcelable.Creator<MovieEntity> CREATOR = new Parcelable.Creator<MovieEntity>() {
        @Override
        public MovieEntity createFromParcel(Parcel source) {
            return new MovieEntity(source);
        }

        @Override
        public MovieEntity[] newArray(int size) {
            return new MovieEntity[size];
        }
    };
}
