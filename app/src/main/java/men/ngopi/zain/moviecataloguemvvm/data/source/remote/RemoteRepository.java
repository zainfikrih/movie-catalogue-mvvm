package men.ngopi.zain.moviecataloguemvvm.data.source.remote;

import java.util.List;

import men.ngopi.zain.moviecataloguemvvm.data.source.remote.response.MovieResponse;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.response.TvShowResponse;

public class RemoteRepository {
    private static RemoteRepository INSTANCE;
    private ApiHelper apiHelper;

    private RemoteRepository(ApiHelper apiHelper) {
        this.apiHelper = apiHelper;
    }

    public static RemoteRepository getInstance(ApiHelper apiHelper) {
        if (INSTANCE == null) {
            INSTANCE = new RemoteRepository(apiHelper);
        }
        return INSTANCE;
    }

    public void getMovies(GetMoviesCallback callback) {
        callback.onMoviesReceived(apiHelper.getMovies(new ApiHelper.GetApiMovieCallback() {
            @Override
            public void onSuccess(List<MovieResponse> movieResponses) {
                callback.onMoviesReceived(movieResponses);
            }

            @Override
            public void onFailure(String errorMessage) {
                callback.onDataNotAvailable();
            }
        }));
    }

    public void getTvShows(GetTvShowsCallback callback) {
        callback.onTvShowsReceived(apiHelper.getTvShows(new ApiHelper.GetApiTvShowCallback() {
            @Override
            public void onSuccess(List<TvShowResponse> tvShowResponses) {
                callback.onTvShowsReceived(tvShowResponses);
            }

            @Override
            public void onFailure(String errorMessage) {
                callback.onDataNotAvailable();
            }
        }));
    }

    public interface GetMoviesCallback {
        void onMoviesReceived(List<MovieResponse> movieResponse);

        void onDataNotAvailable();
    }

    public interface GetTvShowsCallback {
        void onTvShowsReceived(List<TvShowResponse> tvShowResponse);

        void onDataNotAvailable();
    }
}
