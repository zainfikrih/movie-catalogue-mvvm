package men.ngopi.zain.moviecataloguemvvm.data.source;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import men.ngopi.zain.moviecataloguemvvm.data.source.local.LocalRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.RemoteRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.response.MovieResponse;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.response.TvShowResponse;

public class MovieCatalogueRepository implements MovieCatalogueDataSource {
    private volatile static MovieCatalogueRepository INSTANCE = null;

    private final LocalRepository localRepository;
    private final RemoteRepository remoteRepository;

    private MovieCatalogueRepository(@NonNull LocalRepository localRepository, @NonNull RemoteRepository remoteRepository) {
        this.localRepository = localRepository;
        this.remoteRepository = remoteRepository;
    }

    public static MovieCatalogueRepository getInstance(LocalRepository localRepository, RemoteRepository remoteData) {
        if (INSTANCE == null) {
            synchronized (MovieCatalogueRepository.class) {
                if (INSTANCE == null) {
                    INSTANCE = new MovieCatalogueRepository(localRepository, remoteData);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public LiveData<List<MovieEntity>> getMovies() {
        MutableLiveData<List<MovieEntity>> movieResults = new MutableLiveData<>();

        remoteRepository.getMovies(new RemoteRepository.GetMoviesCallback() {
            @Override
            public void onMoviesReceived(List<MovieResponse> movieResponse) {
                Log.d("Repository getMovies", String.valueOf(movieResponse.size()));

                String json = new Gson().toJson(movieResponse);
                Type listType = new TypeToken<List<MovieEntity>>() {
                }.getType();
                List<MovieEntity> movieEntityList;
                movieEntityList = new Gson().fromJson(json, listType);

                movieResults.postValue(movieEntityList);
            }

            @Override
            public void onDataNotAvailable() {

            }
        });
        return movieResults;
    }

    @Override
    public LiveData<List<TvShowEntity>> getTvShows() {
        MutableLiveData<List<TvShowEntity>> tvShowResults = new MutableLiveData<>();

        remoteRepository.getTvShows(new RemoteRepository.GetTvShowsCallback() {
            @Override
            public void onTvShowsReceived(List<TvShowResponse> tvShowResponse) {
                String json = new Gson().toJson(tvShowResponse);
                Type listType = new TypeToken<List<TvShowEntity>>() {
                }.getType();
                List<TvShowEntity> tvShowEntityList;
                tvShowEntityList = new Gson().fromJson(json, listType);

                tvShowResults.postValue(tvShowEntityList);
            }

            @Override
            public void onDataNotAvailable() {

            }
        });
        return tvShowResults;
    }

    @Override
    public LiveData<PagedList<MovieEntity>> getMoviesFavoritePaged() {
        return new LivePagedListBuilder<>(localRepository.getMoviesFavorite(), /* page size */ 10).build();
    }

    @Override
    public LiveData<PagedList<TvShowEntity>> getTvShowsFavoritePaged() {
        return new LivePagedListBuilder<>(localRepository.getTvShowsFavorite(), /* page size */ 10).build();
    }

    public LiveData<MovieEntity> checkMovie(int id) {
        return localRepository.chechMovie(id);
    }

    public LiveData<TvShowEntity> checkTvShow(int id) {
        return localRepository.chechTvShow(id);
    }

    @Override
    public void insertMovie(MovieEntity movieEntity) {
        try {
            new Thread(() -> localRepository.insertMovie(movieEntity)).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertTvShow(TvShowEntity tvShowEntity) {
        try {
            new Thread(() -> localRepository.insertTvShow(tvShowEntity)).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteMovie(MovieEntity movieEntity) {
        try {
            new Thread(() -> localRepository.deleteMovie(movieEntity)).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteTvShow(TvShowEntity tvShowEntity) {
        try {
            new Thread(() -> localRepository.deleteTvShow(tvShowEntity)).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
