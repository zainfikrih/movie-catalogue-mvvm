package men.ngopi.zain.moviecataloguemvvm.di;

import android.app.Application;

import men.ngopi.zain.moviecataloguemvvm.data.source.MovieCatalogueRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.LocalRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.room.MovieCatalogueDatabase;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.ApiHelper;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.RemoteRepository;

public class Injection {
    public static MovieCatalogueRepository provideRepository(Application application) {

        MovieCatalogueDatabase database = MovieCatalogueDatabase.getInstance(application);

        LocalRepository localRepository = LocalRepository.getInstance(database.movieCatalogueDao());
        RemoteRepository remoteRepository = RemoteRepository.getInstance(new ApiHelper(application));

        return MovieCatalogueRepository.getInstance(localRepository, remoteRepository);
    }
}
