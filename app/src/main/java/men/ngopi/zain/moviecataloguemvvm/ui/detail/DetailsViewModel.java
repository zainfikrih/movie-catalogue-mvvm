package men.ngopi.zain.moviecataloguemvvm.ui.detail;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import men.ngopi.zain.moviecataloguemvvm.data.source.MovieCatalogueRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;

public class DetailsViewModel<T> extends ViewModel {
    private T data;
    private MovieCatalogueRepository movieCatalogueRepository;

    public DetailsViewModel(MovieCatalogueRepository mMovieCatalogueRepository) {
        this.movieCatalogueRepository = mMovieCatalogueRepository;
    }

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    LiveData<MovieEntity> checkMovie(int id) {
        return movieCatalogueRepository.checkMovie(id);
    }

    LiveData<TvShowEntity> checkTvShow(int id) {
        return movieCatalogueRepository.checkTvShow(id);
    }

    void insertMovie(MovieEntity movieEntity) {
        movieCatalogueRepository.insertMovie(movieEntity);
    }

    void insertTvShow(TvShowEntity tvShowEntity) {
        movieCatalogueRepository.insertTvShow(tvShowEntity);
    }

    void deleteMovie(MovieEntity movieEntity) {
        movieCatalogueRepository.deleteMovie(movieEntity);
    }

    void deleteTvShow(TvShowEntity tvShowEntity) {
        movieCatalogueRepository.deleteTvShow(tvShowEntity);
    }
}
