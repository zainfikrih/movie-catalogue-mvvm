package men.ngopi.zain.moviecataloguemvvm.ui.favorite.tvshow;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.card.MaterialCardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecataloguemvvm.R;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;
import men.ngopi.zain.moviecataloguemvvm.ui.detail.DetailsActivity;

public class TvShowsFavoriteAdapter extends PagedListAdapter<TvShowEntity, TvShowsFavoriteAdapter.FavoriteViewHolder> {

    TvShowsFavoriteAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new FavoriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteViewHolder holder, int position) {
        TvShowEntity tvShow = getItem(position);
        if (tvShow != null) {
            // Setup url
            String urlBackdrop = "https://image.tmdb.org/t/p/w500" + tvShow.getBackdrop_path();
            String urlPoster = "https://image.tmdb.org/t/p/w500" + tvShow.getPoster_path();

            if (tvShow.getBackdrop_path() != null) {
                // Set image backdrop
                Glide.with(holder.itemView.getContext())
                        .load(urlBackdrop)
                        .apply(new RequestOptions().override(500, 200))
                        .centerCrop()
                        .timeout(3000)
                        .into(holder.ivBackdrop);
            }

            if (tvShow.getPoster_path() != null) {
                // Set image psoter
                Glide.with(holder.itemView.getContext())
                        .load(urlPoster)
                        .centerCrop()
                        .timeout(3000)
                        .into(holder.ivPoster);
            }


            try {
                // Set tv show name
                holder.tvTitle.setText(tvShow.getName());
                // Set tv show year
                String date = tvShow.getFirst_air_date();
                String year = date.substring(0, 4);
                holder.tvYear.setText(year);
            } catch (Exception ignored) {

            }

            holder.cardView.setOnClickListener(view -> {
                Intent detailsMovieIntent = new Intent(holder.itemView.getContext(), DetailsActivity.class);
                detailsMovieIntent.putExtra(DetailsActivity.EXTRA_DETAILS, tvShow);
                holder.itemView.getContext().startActivity(detailsMovieIntent);
            });
        }
    }

    private static DiffUtil.ItemCallback<TvShowEntity> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<TvShowEntity>() {
                @Override
                public boolean areItemsTheSame(@NonNull TvShowEntity oldItem, @NonNull TvShowEntity newItem) {
                    return oldItem.getId() == (newItem.getId());
                }

                @SuppressLint("DiffUtilEquals")
                @Override
                public boolean areContentsTheSame(@NonNull TvShowEntity oldItem, @NonNull TvShowEntity newItem) {
                    return oldItem.equals(newItem);
                }
            };


    class FavoriteViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_backdrop_item_movie)
        ImageView ivBackdrop;
        @BindView(R.id.img_poster_item_movie)
        ImageView ivPoster;
        @BindView(R.id.tv_title_item_movie)
        TextView tvTitle;
        @BindView(R.id.tv_year_item_movie)
        TextView tvYear;
        @BindView(R.id.movie_card)
        MaterialCardView cardView;

        FavoriteViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
