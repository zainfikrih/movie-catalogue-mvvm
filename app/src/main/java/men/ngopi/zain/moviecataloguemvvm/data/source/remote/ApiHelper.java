package men.ngopi.zain.moviecataloguemvvm.data.source.remote;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import men.ngopi.zain.moviecataloguemvvm.BuildConfig;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.response.MovieResponse;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.response.TvShowResponse;
import men.ngopi.zain.moviecataloguemvvm.utils.EspressoIdlingResource;

public class ApiHelper {
    @SuppressLint("StaticFieldLeak")
    private static String apiKey = BuildConfig.TMDB_API_KEY;
    private final static String TAG = ApiHelper.class.getSimpleName();

    private List<MovieResponse> movies = new ArrayList<>();
    private List<TvShowResponse> tvShows = new ArrayList<>();

    public ApiHelper(Application application) {
        AndroidNetworking.initialize(application.getApplicationContext());
    }

    // Get Movies (TMDB API)
    List<MovieResponse> getMovies(GetApiMovieCallback callback) {
        EspressoIdlingResource.increment();

        AndroidNetworking.get("https://api.themoviedb.org/3/discover/movie?api_key=" + apiKey)
                .setPriority(Priority.HIGH)
                .setTag("get_movies")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        try {

                            // Response JSON
                            jsonArray = response.getJSONArray("results");

                            // Gson JSONArrayList to ArrayList<Movie>
                            Type listType = new TypeToken<List<MovieResponse>>() {
                            }.getType();
                            List<MovieResponse> movieEntities;
                            movieEntities = new Gson().fromJson(jsonArray.toString(), listType);

                            movies.clear();
                            movies.addAll(movieEntities);

                            Log.d("ApiHelper getMovies", String.valueOf(movies.size()));

                            callback.onSuccess(movies);

                            EspressoIdlingResource.decrement();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            EspressoIdlingResource.decrement();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("Server", anError.getErrorBody());
                        callback.onFailure(anError.getMessage());
                        EspressoIdlingResource.decrement();

                    }
                });
        return movies;
    }

    // Get Tv Show (TMDB API)
    List<TvShowResponse> getTvShows(GetApiTvShowCallback callback) {
        EspressoIdlingResource.increment();

        AndroidNetworking.get("https://api.themoviedb.org/3/discover/tv?api_key=" + apiKey)
                .setPriority(Priority.HIGH)
                .setTag("get_tv_show")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        try {

                            // Response JSON
                            jsonArray = response.getJSONArray("results");

                            // Gson JSONArrayList to ArrayList<Movie>
                            Type listType = new TypeToken<List<TvShowResponse>>() {
                            }.getType();
                            List<TvShowResponse> tvShow = new Gson().fromJson(jsonArray.toString(), listType);

                            tvShows.clear();
                            tvShows.addAll(tvShow);

                            callback.onSuccess(tvShows);

                            EspressoIdlingResource.decrement();

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.i(TAG, "Catch Show TV Show" + e.getMessage());
                            EspressoIdlingResource.decrement();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.i(TAG, "Catch Show TV Show onError " + anError.getResponse() + " message " + anError.getErrorCode());
                        callback.onFailure(anError.getMessage());
                        EspressoIdlingResource.decrement();

                    }
                });
        return tvShows;
    }

    public interface GetApiMovieCallback {
        void onSuccess(List<MovieResponse> movieResponses);

        void onFailure(String errorMessage);
    }

    public interface GetApiTvShowCallback {
        void onSuccess(List<TvShowResponse> tvShowResponses);

        void onFailure(String errorMessage);
    }
}
