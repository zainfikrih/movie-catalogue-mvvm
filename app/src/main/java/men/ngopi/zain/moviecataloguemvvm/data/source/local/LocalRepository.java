package men.ngopi.zain.moviecataloguemvvm.data.source.local;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;

import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.room.MovieCatalogueDao;

public class LocalRepository {
    private final MovieCatalogueDao dao;

    private LocalRepository(MovieCatalogueDao dao) {
        this.dao = dao;
    }

    private static LocalRepository INSTANCE;

    public static LocalRepository getInstance(MovieCatalogueDao dao) {
        if (INSTANCE == null) {
            INSTANCE = new LocalRepository(dao);
        }
        return INSTANCE;
    }

    public DataSource.Factory<Integer, MovieEntity> getMoviesFavorite() {
        return dao.getMoviesFavorite();
    }

    public DataSource.Factory<Integer, TvShowEntity> getTvShowsFavorite() {
        return dao.getTvShowsFavorite();
    }

    public LiveData<MovieEntity> chechMovie(int id){
        return dao.checkMovie(id);
    }

    public LiveData<TvShowEntity> chechTvShow(int id){
        return dao.checkTvShow(id);
    }

    public void insertMovie(MovieEntity movieEntity) {
        dao.insertMovie(movieEntity);
    }

    public void insertTvShow(TvShowEntity tvShowEntity) {
        dao.insertTvShow(tvShowEntity);
    }

    public void deleteMovie(MovieEntity movieEntity) {
        dao.deleteMovie(movieEntity);
    }

    public void deleteTvShow(TvShowEntity tvShowEntity) {
        dao.deleteTvShow(tvShowEntity);
    }
}
