package men.ngopi.zain.moviecataloguemvvm.data.source.local.room;

import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;

@Dao
public interface MovieCatalogueDao {

    @WorkerThread
    @Query("SELECT * FROM movieentity")
    DataSource.Factory<Integer, MovieEntity> getMoviesFavorite();

    @WorkerThread
    @Query("SELECT * FROM tvshowentity")
    DataSource.Factory<Integer, TvShowEntity> getTvShowsFavorite();

    @Transaction
    @Query("SELECT * FROM movieentity WHERE id = :id")
    LiveData<MovieEntity> checkMovie(int id);

    @Transaction
    @Query("SELECT * FROM tvshowentity WHERE id = :id")
    LiveData<TvShowEntity> checkTvShow(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMovie(MovieEntity movieEntity);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTvShow(TvShowEntity tvShowEntity);

    @Delete
    void deleteMovie(MovieEntity movieEntity);

    @Delete
    void deleteTvShow(TvShowEntity tvShowEntity);
}
