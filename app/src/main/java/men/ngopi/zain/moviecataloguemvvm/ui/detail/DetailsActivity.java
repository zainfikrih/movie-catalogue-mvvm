package men.ngopi.zain.moviecataloguemvvm.ui.detail;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.palette.graphics.Palette;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecataloguemvvm.R;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;
import men.ngopi.zain.moviecataloguemvvm.utils.Animate;
import men.ngopi.zain.moviecataloguemvvm.viewmodel.ViewModelFactory;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class DetailsActivity extends AppCompatActivity {
    public static String EXTRA_DETAILS = "details";
    public boolean isMov = true;
    private DetailsViewModel viewModel;
    private MovieEntity movieEntity;
    private TvShowEntity tvShowEntity;

    @BindView(R.id.details_appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.details_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.details_img_toolbar)
    ImageView ivToolbar;
    @BindView(R.id.details_toolbar)
    Toolbar toolbar;
    @BindView(R.id.details_img_poster)
    ImageView ivPoster;
    @BindView(R.id.details_title_expanded)
    TextView tvTitleExpanded;
    @BindView(R.id.details_sub_title_expanded)
    TextView tvSubTitleExpanded;
    @BindView(R.id.details_view_title_expanded)
    LinearLayout viewTitle;

    @BindView(R.id.details_overview)
    TextView tvOverview;
    @BindView(R.id.details_img_poster_overview)
    ImageView ivPosterOverview;
    @BindView(R.id.details_user_score)
    TextView tvUserScore;
    @BindView(R.id.details_release_date)
    TextView tvReleaseDate;
    @BindView(R.id.details_vote)
    TextView tvVote;
    @BindView(R.id.details_language)
    TextView tvLanguage;

    @BindView(R.id.layout_content_details)
    ConstraintLayout layoutDetails;
    @BindView(R.id.layout_content_details_placeholder)
    ConstraintLayout layoutDetailsPlaceHolder;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Bind view
        ButterKnife.bind(this);

        layoutDetails.setVisibility(View.GONE);
        layoutDetailsPlaceHolder.setVisibility(View.GONE);
        ivPosterOverview.setVisibility(View.INVISIBLE);

        viewModel = obtainViewModel(this);

        if (getIntent().getParcelableExtra(EXTRA_DETAILS) instanceof MovieEntity) {
            isMov = true;
            showPlaceHolder();
            movieEntity = getIntent().getParcelableExtra(EXTRA_DETAILS);
            //noinspection unchecked
            viewModel.setData(movieEntity);
            showDetailsMovies((MovieEntity) viewModel.getData());
        } else {
            isMov = false;
            showPlaceHolder();
            tvShowEntity = getIntent().getParcelableExtra(EXTRA_DETAILS);
            //noinspection unchecked
            viewModel.setData(tvShowEntity);
            showDetailsTvShow((TvShowEntity) viewModel.getData());
        }

        // Setup Toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        collapsingToolbarLayout.setTitle("");
    }

    @NonNull
    private static DetailsViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(DetailsViewModel.class);
    }

    private void showPlaceHolder() {
        layoutDetailsPlaceHolder.setVisibility(View.VISIBLE);
        layoutDetails.setVisibility(View.GONE);
    }

    private void hidePlaceHolder() {
        layoutDetailsPlaceHolder.setVisibility(View.GONE);
        layoutDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void showDetailsMovies(MovieEntity movie) {
        hidePlaceHolder();
        // Set Movie Name
        String movieName;
        if (movie.getTitle() == null) {
            movieName = movie.getOriginal_name();
        } else {
            movieName = movie.getTitle();
        }

        // Set Movie Date, year
        String movieYear;
        String movieDate;
        if (movie.getRelease_date() == null) {
            movieDate = movie.getFirst_air_date();
            movieYear = movieDate.substring(0, 4);
        } else {
            movieDate = movie.getRelease_date();
            movieYear = movieDate.substring(0, 4);
        }

        // Set TextView (Movie)
        tvTitleExpanded.setText(movieName);
        tvSubTitleExpanded.setText("(" + movieYear + ")");
        tvOverview.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        tvOverview.setText(movie.getOverview());
        tvUserScore.setText(String.valueOf(movie.getVote_average()));
        tvReleaseDate.setText(movieDate);
        tvVote.setText(String.valueOf(movie.getVote_count()));
        tvLanguage.setText(movie.getOriginal_language());

        // Set Poster
        setPoster(movie.getPoster_path());

        // Set Color Toolbar
        setColorToolbar(movie.getBackdrop_path());

        myAppBar(movieName);
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void showDetailsTvShow(TvShowEntity tvShow) {
        hidePlaceHolder();

        String tvShowDate = tvShow.getFirst_air_date();
        String tvShowYear = tvShowDate.substring(0, 4);

        // Set TextView (Movie)
        tvTitleExpanded.setText(tvShow.getName());
        tvSubTitleExpanded.setText("(" + tvShowYear + ")");
        tvOverview.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        tvOverview.setText(tvShow.getOverview());
        tvUserScore.setText(String.valueOf(tvShow.getVote_average()));
        tvReleaseDate.setText(tvShowDate);
        tvVote.setText(String.valueOf(tvShow.getVote_count()));
        tvLanguage.setText(tvShow.getOriginal_language());

        // Set Poster
        setPoster(tvShow.getPoster_path());

        // Set Color Toolbar
        setColorToolbar(tvShow.getBackdrop_path());

        myAppBar(tvShow.getName());
    }

    private void setPoster(String poster) {
        String url = "https://image.tmdb.org/t/p/w500" + poster;
        ivPoster.setTag(poster);

        //noinspection deprecation
        Glide.with(this)
                .load(url)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {

                        // Set Image Poster CollapsingTollbar
                        ivPoster.setImageDrawable(resource);

                        // Set Image Poster
                        ivPosterOverview.setImageDrawable(resource);
                    }
                });
    }

    private void setColorToolbar(String backdrop) {
        String url = "https://image.tmdb.org/t/p/w500" + backdrop;
        ivToolbar.setTag(backdrop);

        //noinspection deprecation
        Glide.with(this)
                .load(url)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @SuppressLint("InlinedApi")
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {

                        // Set Image Toolbar
                        ivToolbar.setImageDrawable(resource);

                        // Set Color Palette Toolbar & StatusBar
                        Bitmap bm = ((BitmapDrawable) ivToolbar.getDrawable()).getBitmap();
                        Palette.from(bm).generate(palette -> {
                            assert palette != null;
                            int mutedColor = palette.getDominantColor(R.attr.colorAccent); //Muted, Dark, Light, Vibrant
                            collapsingToolbarLayout.setContentScrimColor(mutedColor);

                            // StatusBar
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Window window = getWindow();
                                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                window.setStatusBarColor(mutedColor);
                                int flags = window.getDecorView().getSystemUiVisibility();
                                flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                                window.getDecorView().setSystemUiVisibility(flags);
                            }
                        });
                    }
                });

    }

    private void myAppBar(final String title) {
        // Listener AppBarLayout
        appBarLayout.addOnOffsetChangedListener((appBarLayout, i) -> {
            if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
                // Collapsed
                Animate.show(ivPosterOverview, View.VISIBLE);
                Animate.hide(ivPoster, View.GONE);
                Animate.hide(viewTitle, View.GONE);
                collapsingToolbarLayout.setTitle(title);
            } else {
                // Expanded
                Animate.hide(ivPosterOverview, View.INVISIBLE);
                Animate.show(ivPoster, View.VISIBLE);
                Animate.show(viewTitle, View.VISIBLE);
                collapsingToolbarLayout.setTitle("");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        MenuItem item = menu.getItem(0);
        if (isMov) {
            //noinspection unchecked
            viewModel.checkMovie(movieEntity.getId()).observe(this, movieEntity -> {
                if (movieEntity != null) {
                    item.setChecked(true);
                    favorite(item);
                } else {
                    item.setChecked(false);
                    favorite(item);
                }
            });
        } else {
            //noinspection unchecked
            viewModel.checkTvShow(tvShowEntity.getId()).observe(this, tvShowEntity -> {
                if (tvShowEntity != null) {
                    item.setChecked(true);
                    favorite(item);
                } else {
                    item.setChecked(false);
                    favorite(item);
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_detail_favorite) {
            if (item.isChecked()) {
                if (isMov) {
                    viewModel.insertMovie(movieEntity);
                } else {
                    viewModel.insertTvShow(tvShowEntity);
                }
            } else {
                if (isMov) {
                    viewModel.deleteMovie(movieEntity);
                } else {
                    viewModel.deleteTvShow(tvShowEntity);
                }
            }
            favorite(item);
        }
        return super.onOptionsItemSelected(item);
    }

    private void favorite(MenuItem item) {
        if (item.isChecked()) {
            item.setChecked(false);
            item.setIcon(R.drawable.ic_favorite_white);
        } else {
            item.setChecked(true);
            item.setIcon(R.drawable.ic_favorite_border);
        }
    }
}
