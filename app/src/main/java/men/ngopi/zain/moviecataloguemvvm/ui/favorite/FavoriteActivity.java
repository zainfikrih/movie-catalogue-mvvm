package men.ngopi.zain.moviecataloguemvvm.ui.favorite;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecataloguemvvm.R;
import men.ngopi.zain.moviecataloguemvvm.ui.favorite.movies.MoviesFavoriteFragment;
import men.ngopi.zain.moviecataloguemvvm.ui.favorite.tvshow.TvShowFavoriteFragment;

public class FavoriteActivity extends AppCompatActivity {

    @BindView(R.id.fav_toolbar)
    Toolbar toolbar;
    @BindView(R.id.fav_view_pager)
    ViewPager viewPager;
    @BindView(R.id.fav_tab_layout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        ButterKnife.bind(this);

        setupViewPager();

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }


    public void setupViewPager() {
        FavoriteActivity.PagerAdapter pagerAdapter = new FavoriteActivity.PagerAdapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private class PagerAdapter extends FragmentPagerAdapter {

        PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        private int count = 2;

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return MoviesFavoriteFragment.newInstance(null);
                case 1:
                    return TvShowFavoriteFragment.newInstance(null);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return count;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.tab1);
                case 1:
                    return getString(R.string.tab2);
                default:
                    return null;
            }
        }
    }

}
