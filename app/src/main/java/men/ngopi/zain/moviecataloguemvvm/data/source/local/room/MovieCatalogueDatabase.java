package men.ngopi.zain.moviecataloguemvvm.data.source.local.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;

@Database(entities = {MovieEntity.class, TvShowEntity.class},
        version = 1,
        exportSchema = false)
public abstract class MovieCatalogueDatabase extends RoomDatabase {

    private static MovieCatalogueDatabase INSTANCE;

    public abstract MovieCatalogueDao movieCatalogueDao();

    private static final Object sLock = new Object();

    public static MovieCatalogueDatabase getInstance(Context context) {
        synchronized (sLock) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        MovieCatalogueDatabase.class, "Moviecatalogue.db")
                        .build();
            }
            return INSTANCE;
        }
    }

}
