package men.ngopi.zain.moviecataloguemvvm.ui.favorite;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.PagedList;

import men.ngopi.zain.moviecataloguemvvm.data.source.MovieCatalogueRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;

public class FavoriteViewModel extends ViewModel {
    private MovieCatalogueRepository movieCatalogueRepository;

    public FavoriteViewModel(MovieCatalogueRepository mMovieCatalogueRepository) {
        this.movieCatalogueRepository = mMovieCatalogueRepository;
    }

    public LiveData<PagedList<MovieEntity>> getMoviesFavoritePaged() {
        return movieCatalogueRepository.getMoviesFavoritePaged();
    }

    public LiveData<PagedList<TvShowEntity>> getTvShowsFavoritePaged() {
        return movieCatalogueRepository.getTvShowsFavoritePaged();
    }

}
