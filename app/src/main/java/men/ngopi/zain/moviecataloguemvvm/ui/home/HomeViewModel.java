package men.ngopi.zain.moviecataloguemvvm.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import men.ngopi.zain.moviecataloguemvvm.data.source.MovieCatalogueRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;

public class HomeViewModel extends ViewModel {
    private MovieCatalogueRepository movieCatalogueRepository;

    public HomeViewModel(MovieCatalogueRepository mMovieCatalogueRepository) {
        this.movieCatalogueRepository = mMovieCatalogueRepository;
    }

    public LiveData<List<MovieEntity>> getAllMovie() {
        return movieCatalogueRepository.getMovies();
    }

    public LiveData<List<TvShowEntity>> getAllTvShow() {
        return movieCatalogueRepository.getTvShows();
    }

}
