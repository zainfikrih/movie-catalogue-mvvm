package men.ngopi.zain.moviecataloguemvvm.ui.home.movies;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecataloguemvvm.R;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.ui.detail.DetailsActivity;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private Context context;
    private List<MovieEntity> movies;
    private LayoutInflater layoutInflater;

    MovieAdapter(Context context) {
        this.context = context;
        this.movies = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    void setMovies(List<MovieEntity> movies) {
        this.movies.clear();
        this.movies.addAll(movies);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.item_movie, viewGroup, false);
        return new MovieAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        // Setup url
        String urlBackdrop = "https://image.tmdb.org/t/p/w500" + movies.get(i).getBackdrop_path();
        String urlPoster = "https://image.tmdb.org/t/p/w500" + movies.get(i).getPoster_path();

        if (movies.get(i).getBackdrop_path() != null) {
            // Set image backdrop
            Glide.with(context)
                    .load(urlBackdrop)
                    .apply(new RequestOptions().override(500, 200))
                    .centerCrop()
                    .timeout(3000)
                    .into(viewHolder.ivBackdrop);
        }

        if (movies.get(i).getPoster_path() != null) {
            // Set image psoter
            Glide.with(context)
                    .load(urlPoster)
                    .centerCrop()
                    .timeout(3000)
                    .into(viewHolder.ivPoster);
        }


        try {
            // Set movie name
            if (movies.get(i).getTitle() == null) {
                viewHolder.tvTitle.setText(movies.get(i).getOriginal_name());
            } else {
                viewHolder.tvTitle.setText(movies.get(i).getTitle());
            }

            // Set movie year
            if (movies.get(i).getRelease_date() == null) {
                String date = movies.get(i).getFirst_air_date();
                String year = date.substring(0, 4);
                viewHolder.tvYear.setText(year);
            } else {
                String date = movies.get(i).getRelease_date();
                String year = date.substring(0, 4);
                viewHolder.tvYear.setText(year);
            }
        } catch (Exception ignored) {

        }

        viewHolder.cardView.setOnClickListener(view -> {
            int position = viewHolder.getAdapterPosition();
            Intent detailsMovieIntent = new Intent(context, DetailsActivity.class);
            detailsMovieIntent.putExtra(DetailsActivity.EXTRA_DETAILS, movies.get(position));
            context.startActivity(detailsMovieIntent);
        });
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_backdrop_item_movie)
        ImageView ivBackdrop;
        @BindView(R.id.img_poster_item_movie)
        ImageView ivPoster;
        @BindView(R.id.tv_title_item_movie)
        TextView tvTitle;
        @BindView(R.id.tv_year_item_movie)
        TextView tvYear;
        @BindView(R.id.movie_card)
        MaterialCardView cardView;

        ViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
