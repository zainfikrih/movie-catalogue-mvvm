package men.ngopi.zain.moviecataloguemvvm.ui.home.movies;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecataloguemvvm.R;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.ui.home.HomeViewModel;
import men.ngopi.zain.moviecataloguemvvm.viewmodel.ViewModelFactory;

public class MoviesFragment extends Fragment {
    @BindView(R.id.rv_movies)
    RecyclerView recyclerView;
    @BindView(R.id.pb_movies)
    ProgressBar pbMovies;
    @BindView(R.id.no_movies)
    ImageView ivNoMovies;

    private MovieAdapter adapter;
    private List<MovieEntity> movies = new ArrayList<>();

    public MoviesFragment() {

    }

    public static MoviesFragment newInstance(Bundle bundle) {
        MoviesFragment moviesFragment = new MoviesFragment();
        moviesFragment.setArguments(bundle);
        return moviesFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            HomeViewModel viewModel = obtainViewModel(getActivity());
            adapter = new MovieAdapter(getActivity());

            viewModel.getAllMovie().observe(this, movieEntities -> {
                if (movieEntities != null) {
                    if (movieEntities.size() != 0) {
                        pbMovies.setVisibility(View.GONE);
                        movies.clear();
                        movies.addAll(movieEntities);
                        Log.d("Movies", String.valueOf(movies.size()));

                        adapter.setMovies(movies);
                        adapter.notifyDataSetChanged();
                    }
                }
            });

            // Set adapter to listview
            LinearLayoutManager llm = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(llm);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @NonNull
    private static HomeViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(HomeViewModel.class);
    }

}
