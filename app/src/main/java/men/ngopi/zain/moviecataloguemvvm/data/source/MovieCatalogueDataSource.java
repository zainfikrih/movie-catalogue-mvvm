package men.ngopi.zain.moviecataloguemvvm.data.source;

import androidx.lifecycle.LiveData;
import androidx.paging.PagedList;

import java.util.List;

import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;

public interface MovieCatalogueDataSource {
    LiveData<List<MovieEntity>> getMovies();

    LiveData<List<TvShowEntity>> getTvShows();

    LiveData<PagedList<MovieEntity>> getMoviesFavoritePaged();

    LiveData<PagedList<TvShowEntity>> getTvShowsFavoritePaged();

    void insertMovie(MovieEntity movieEntity);

    void insertTvShow(TvShowEntity tvShowEntity);

    void deleteMovie(MovieEntity movieEntity);

    void deleteTvShow(TvShowEntity tvShowEntity);
}

