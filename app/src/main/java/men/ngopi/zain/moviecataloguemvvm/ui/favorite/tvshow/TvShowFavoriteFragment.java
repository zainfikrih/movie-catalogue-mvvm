package men.ngopi.zain.moviecataloguemvvm.ui.favorite.tvshow;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecataloguemvvm.R;
import men.ngopi.zain.moviecataloguemvvm.ui.favorite.FavoriteViewModel;
import men.ngopi.zain.moviecataloguemvvm.viewmodel.ViewModelFactory;

public class TvShowFavoriteFragment extends Fragment {
    @BindView(R.id.rv_movies)
    RecyclerView recyclerView;
    @BindView(R.id.pb_movies)
    ProgressBar pbMovies;
    @BindView(R.id.no_movies)
    ImageView ivNoMovies;

    public TvShowFavoriteFragment() {

    }

    public static TvShowFavoriteFragment newInstance(Bundle bundle) {
        TvShowFavoriteFragment tvShowFavoriteFragment = new TvShowFavoriteFragment();
        tvShowFavoriteFragment.setArguments(bundle);
        return tvShowFavoriteFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            FavoriteViewModel viewModel = obtainViewModel(getActivity());
            TvShowsFavoriteAdapter adapter = new TvShowsFavoriteAdapter();

            viewModel.getTvShowsFavoritePaged().observe(this, tvShowEntities -> {
                if (tvShowEntities != null) {
                    pbMovies.setVisibility(View.GONE);
                    if (!tvShowEntities.isEmpty()) {
                        ivNoMovies.setVisibility(View.GONE);
                    } else {
                        ivNoMovies.setVisibility(View.VISIBLE);
                    }

                    adapter.submitList(tvShowEntities);
                    adapter.notifyDataSetChanged();
                }
            });


            // Set adapter to listview
            LinearLayoutManager llm = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            recyclerView.setLayoutManager(llm);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);

        }
    }

    @NonNull
    private static FavoriteViewModel obtainViewModel(FragmentActivity activity) {
        // Use a Factory to inject dependencies into the ViewModel
        ViewModelFactory factory = ViewModelFactory.getInstance(activity.getApplication());
        return ViewModelProviders.of(activity, factory).get(FavoriteViewModel.class);
    }
}
