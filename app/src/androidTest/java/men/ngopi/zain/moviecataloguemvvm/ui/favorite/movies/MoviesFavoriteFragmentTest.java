package men.ngopi.zain.moviecataloguemvvm.ui.favorite.movies;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import men.ngopi.zain.moviecataloguemvvm.R;
import men.ngopi.zain.moviecataloguemvvm.testing.SingleFragmentActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class MoviesFavoriteFragmentTest {
    @Rule
    public ActivityTestRule<SingleFragmentActivity> activityActivityTestRule = new ActivityTestRule<>(SingleFragmentActivity.class);
    private MoviesFavoriteFragment moviesFragment = new MoviesFavoriteFragment();

    @Before
    public void setUp() {
        activityActivityTestRule.getActivity().setFragment(moviesFragment);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void loadMovies() {
        onView(withId(R.id.rv_movies)).check(matches(isDisplayed()));
//        onView(withId(R.id.rv_movies)).check(new RecyclerViewItemCountAssertion(20));
    }
}