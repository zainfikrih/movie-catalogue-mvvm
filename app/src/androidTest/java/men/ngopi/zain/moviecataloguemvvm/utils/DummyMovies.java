package men.ngopi.zain.moviecataloguemvvm.utils;

import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;

public class DummyMovies {

    public static MovieEntity generateMovie(){
        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setOriginal_name("The Lion King");
        movieEntity.setRelease_date("2019-10-10");
        movieEntity.setOverview("Simba idolises his father");
        movieEntity.setVote_average(7.2);
        movieEntity.setVote_count(2090);
        movieEntity.setOriginal_language("en");
        movieEntity.setBackdrop_path("/1TUg5pO1VZ4B0Q1amk3OlXvlpXV.jpg");
        movieEntity.setPoster_path("/dzBtMocZuJbjLOXvrl4zGYigDzh.jpg");
        return movieEntity;
    }
}