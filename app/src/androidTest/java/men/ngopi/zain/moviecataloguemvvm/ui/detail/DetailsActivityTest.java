package men.ngopi.zain.moviecataloguemvvm.ui.detail;

import android.content.Context;
import android.content.Intent;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import men.ngopi.zain.moviecataloguemvvm.R;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.utils.DummyMovies;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withTagValue;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.equalTo;

public class DetailsActivityTest {

    private MovieEntity movieEntity = DummyMovies.generateMovie();

    @Rule
    public ActivityTestRule<DetailsActivity> activityRule = new ActivityTestRule<DetailsActivity>(DetailsActivity.class) {
        @Override
        protected Intent getActivityIntent() {
            Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
            Intent result = new Intent(targetContext, DetailsActivity.class);
            result.putExtra(DetailsActivity.EXTRA_DETAILS, movieEntity);
            return result;
        }
    };

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void loadDetailsMovie() {
        //backdrop
        onView(withId(R.id.details_img_toolbar)).check(matches(isDisplayed()));
        onView(withId(R.id.details_img_toolbar)).check(matches(withTagValue(equalTo(movieEntity.getBackdrop_path()))));
        //poster
        onView(withId(R.id.details_img_poster)).check(matches(isDisplayed()));
        onView(withId(R.id.details_img_poster)).check(matches(withTagValue(equalTo(movieEntity.getPoster_path()))));
        //title
        onView(withId(R.id.details_title_expanded)).check(matches(isDisplayed()));
        onView(withId(R.id.details_title_expanded)).check(matches(withText(movieEntity.getOriginal_name())));
        //year
        onView(withId(R.id.details_sub_title_expanded)).check(matches(isDisplayed()));
        onView(withId(R.id.details_sub_title_expanded)).check(matches(withText("(" + movieEntity.getRelease_date().substring(0, 4) + ")")));
        //overview
        onView(withId(R.id.details_overview)).check(matches(isDisplayed()));
        onView(withId(R.id.details_overview)).check(matches(withText(movieEntity.getOverview())));
        //user score
        onView(withId(R.id.details_user_score)).check(matches(isDisplayed()));
        onView(withId(R.id.details_user_score)).check(matches(withText(String.valueOf(movieEntity.getVote_average()))));
        //release date
        onView(withId(R.id.details_release_date)).check(matches(isDisplayed()));
        onView(withId(R.id.details_release_date)).check(matches(withText(movieEntity.getRelease_date())));
        //vote count
        onView(withId(R.id.details_vote)).check(matches(isDisplayed()));
        onView(withId(R.id.details_vote)).check(matches(withText(String.valueOf(movieEntity.getVote_count()))));
        //language
        onView(withId(R.id.details_language)).check(matches(isDisplayed()));
        onView(withId(R.id.details_language)).check(matches(withText(String.valueOf(movieEntity.getOriginal_language()))));

    }
}