package men.ngopi.zain.moviecataloguemvvm.ui.favorite.tvshow;

import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import men.ngopi.zain.moviecataloguemvvm.R;
import men.ngopi.zain.moviecataloguemvvm.testing.SingleFragmentActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class TvShowFavoriteFragmentTest {
    @Rule
    public ActivityTestRule<SingleFragmentActivity> activityActivityTestRule = new ActivityTestRule<>(SingleFragmentActivity.class);
    private TvShowFavoriteFragment tvShowFavoriteFragment = new TvShowFavoriteFragment();

    @Before
    public void setUp() {
        activityActivityTestRule.getActivity().setFragment(tvShowFavoriteFragment);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void loadMovies() {
        onView(withId(R.id.rv_movies)).check(matches(isDisplayed()));
//        onView(withId(R.id.rv_movies)).check(new RecyclerViewItemCountAssertion(20));
    }
}