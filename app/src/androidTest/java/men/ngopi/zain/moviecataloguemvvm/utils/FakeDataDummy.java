package men.ngopi.zain.moviecataloguemvvm.utils;

import java.util.ArrayList;

import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;

public class FakeDataDummy {
    public static ArrayList<MovieEntity> generateMovie() {
        ArrayList<MovieEntity> movieEntities;
        movieEntities = new ArrayList<>();

        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setOriginal_name("The Lion King");
        movieEntity.setRelease_date("2019-10-10");
        movieEntity.setOverview("Simba idolises his father");
        movieEntity.setVote_average(7.2);
        movieEntity.setVote_count(2090);
        movieEntity.setOriginal_language("en");
        movieEntity.setBackdrop_path("/1TUg5pO1VZ4B0Q1amk3OlXvlpXV.jpg");
        movieEntity.setPoster_path("/dzBtMocZuJbjLOXvrl4zGYigDzh.jpg");

        movieEntities.add(movieEntity);
        movieEntities.add(movieEntity);

        return movieEntities;
    }

    public static ArrayList<TvShowEntity> generateTvShow() {
        ArrayList<TvShowEntity> tvShowEntities;
        tvShowEntities = new ArrayList<>();

        TvShowEntity tvShowEntity= new TvShowEntity();
        tvShowEntity.setOriginal_name("The Lion King");
        tvShowEntity.setFirst_air_date("2019-10-10");
        tvShowEntity.setOverview("Simba idolises his father");
        tvShowEntity.setVote_average(7.2);
        tvShowEntity.setVote_count(2090);
        tvShowEntity.setOriginal_language("en");
        tvShowEntity.setBackdrop_path("/1TUg5pO1VZ4B0Q1amk3OlXvlpXV.jpg");
        tvShowEntity.setPoster_path("/dzBtMocZuJbjLOXvrl4zGYigDzh.jpg");

        tvShowEntities.add(tvShowEntity);
        tvShowEntities.add(tvShowEntity);

        return tvShowEntities;
    }
}
