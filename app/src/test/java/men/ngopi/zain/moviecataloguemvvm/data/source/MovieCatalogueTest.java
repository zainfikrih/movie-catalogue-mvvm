package men.ngopi.zain.moviecataloguemvvm.data.source;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.paging.DataSource;
import androidx.paging.PagedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Objects;

import men.ngopi.zain.moviecataloguemvvm.data.source.local.LocalRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.RemoteRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.remote.response.MovieResponse;
import men.ngopi.zain.moviecataloguemvvm.utils.FakeDataDummy;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MovieCatalogueTest {
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private LocalRepository local = mock(LocalRepository.class);
    private RemoteRepository remote = mock(RemoteRepository.class);
    private FakeMovieCatalogueRepository repository = new FakeMovieCatalogueRepository(local, remote);

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void getFavoriteMovieTest(){
        DataSource.Factory<Integer, MovieEntity> dataSourceFactory = mock(DataSource.Factory.class);

        when(local.getMoviesFavorite()).thenReturn(dataSourceFactory);

        repository.getMoviesFavoritePaged();

        verify(local).getMoviesFavorite();
    }

    @Test
    public void getFavoriteTvShowTest(){
        DataSource.Factory<Integer, TvShowEntity> dataSourceFactory = mock(DataSource.Factory.class);

        when(local.getTvShowsFavorite()).thenReturn(dataSourceFactory);

        repository.getTvShowsFavoritePaged();

        verify(local).getTvShowsFavorite();
    }
}
