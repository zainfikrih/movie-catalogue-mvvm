package men.ngopi.zain.moviecataloguemvvm.ui.favorite;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.paging.PagedList;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import men.ngopi.zain.moviecataloguemvvm.data.source.MovieCatalogueRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;
import men.ngopi.zain.moviecataloguemvvm.utils.FakeDataDummy;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FavoriteViewModelTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private FavoriteViewModel viewModel;
    private MovieCatalogueRepository repository = mock(MovieCatalogueRepository.class);

    @Before
    public void setUp() {
        viewModel = new FavoriteViewModel(repository);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getFavoriteMovies() {
        MutableLiveData<PagedList<MovieEntity>> movieDummy = new MutableLiveData<>();

        when(repository.getMoviesFavoritePaged()).thenReturn(movieDummy);

        Observer<PagedList<MovieEntity>> observer = mock(Observer.class);

        viewModel.getMoviesFavoritePaged().observeForever(observer);

        verify(repository).getMoviesFavoritePaged();
    }

    @Test
    public void getFavoriteTvShow() {
        MutableLiveData<PagedList<TvShowEntity>> movieDummy = new MutableLiveData<>();

        when(repository.getTvShowsFavoritePaged()).thenReturn(movieDummy);

        Observer<PagedList<TvShowEntity>> observer = mock(Observer.class);

        viewModel.getTvShowsFavoritePaged().observeForever(observer);

        verify(repository).getTvShowsFavoritePaged();
    }

}