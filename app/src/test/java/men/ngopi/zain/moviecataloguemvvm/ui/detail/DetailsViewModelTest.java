package men.ngopi.zain.moviecataloguemvvm.ui.detail;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Objects;

import men.ngopi.zain.moviecataloguemvvm.data.source.MovieCatalogueRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;
import men.ngopi.zain.moviecataloguemvvm.ui.home.HomeViewModel;
import men.ngopi.zain.moviecataloguemvvm.utils.FakeDataDummy;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DetailsViewModelTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private DetailsViewModel<MovieEntity> viewModel;
    private MovieCatalogueRepository repository = mock(MovieCatalogueRepository.class);

    @Before
    public void setUp() {
        viewModel = new DetailsViewModel(repository);
    }

    @After
    public void tearDown(){
    }

    @Test
    public void getData() {
        MovieEntity movieEntity = FakeDataDummy.generateMovie().get(0);
        viewModel.setData(movieEntity);
        String originalName = viewModel.getData().getOriginal_name();
        assertEquals(movieEntity.getOriginal_name(), originalName);
    }

    @Test
    public void checkMovieTest() {
        MutableLiveData<MovieEntity> movieDummy = new MutableLiveData<>();
        movieDummy.setValue(FakeDataDummy.generateMovie().get(0));

        when(repository.checkMovie(Objects.requireNonNull(movieDummy.getValue()).getId())).thenReturn(movieDummy);

        Observer<MovieEntity> observer = Mockito.mock(Observer.class);

        viewModel.checkMovie(movieDummy.getValue().getId()).observeForever(observer);

        verify(repository).checkMovie(movieDummy.getValue().getId());
    }

    @Test
    public void checkTvShowTest() {
        MutableLiveData<TvShowEntity> tvShowDummy = new MutableLiveData<>();
        tvShowDummy.setValue(FakeDataDummy.generateTvShow().get(0));

        when(repository.checkTvShow(Objects.requireNonNull(tvShowDummy.getValue()).getId())).thenReturn(tvShowDummy);

        Observer<TvShowEntity> observer = Mockito.mock(Observer.class);

        viewModel.checkTvShow(tvShowDummy.getValue().getId()).observeForever(observer);

        verify(repository).checkTvShow(tvShowDummy.getValue().getId());
    }
}