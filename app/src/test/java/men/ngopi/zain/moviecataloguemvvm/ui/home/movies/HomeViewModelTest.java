package men.ngopi.zain.moviecataloguemvvm.ui.home.movies;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import men.ngopi.zain.moviecataloguemvvm.data.source.MovieCatalogueRepository;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.MovieEntity;
import men.ngopi.zain.moviecataloguemvvm.data.source.local.entity.TvShowEntity;
import men.ngopi.zain.moviecataloguemvvm.ui.home.HomeViewModel;
import men.ngopi.zain.moviecataloguemvvm.utils.FakeDataDummy;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HomeViewModelTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    private HomeViewModel viewModel;
    private MovieCatalogueRepository repository = mock(MovieCatalogueRepository.class);

    @Before
    public void setUp() {
        viewModel = new HomeViewModel(repository);
    }

    @Test
    public void getMovies() {
        MutableLiveData<List<MovieEntity>> movieDummy = new MutableLiveData<>();
        movieDummy.setValue(FakeDataDummy.generateMovie());

        when(repository.getMovies()).thenReturn(movieDummy);

        Observer<List<MovieEntity>> observer = Mockito.mock(Observer.class);

        viewModel.getAllMovie().observeForever(observer);

        verify(repository).getMovies();
    }

    @Test
    public void getTvShow() {

        MutableLiveData<List<TvShowEntity>> tvShowDummy = new MutableLiveData<>();
        tvShowDummy.setValue(FakeDataDummy.generateTvShow());

        when(repository.getTvShows()).thenReturn(tvShowDummy);

        Observer<List<TvShowEntity>> observer = Mockito.mock(Observer.class);

        viewModel.getAllTvShow().observeForever(observer);

        verify(repository).getTvShows();
    }
}